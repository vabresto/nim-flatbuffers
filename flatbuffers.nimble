# Package

version       = "0.1.0"
author        = "Victor Brestoiu"
description   = "A pure-Nim implementation of Flatbuffers (no gRPC)"
license       = "MIT"
srcDir        = "src"
installExt    = @["nim"]
bin           = @["flatbuffers/private/flatnim"]
binDir        = "bin"
namedBin["flatbuffers/private/flatnim"] = "flatnim"

# Dependencies

requires "nim >= 1.6.0"

# Tasks
task docs, "Build docs":
  if paramCount() == 9:
    exec("nim doc --project --index:on --outdir:" & paramStr(9) & " src/flatbuffers")
    exec("nim doc --project --index:on --outdir:" & paramStr(9) & " src/flatbuffers/private/flatnim")

    let contents = """
    <html>
      <head>
        <meta http-equiv=\"Refresh\" content=\"0; url='https://vabresto.gitlab.io/nim-flatbuffers/flatnim.html'\" />
      </head>
      <body>
        <p>Please see <a href=\"https://vabresto.gitlab.io/nim-flatbuffers/flatnim.html\">this link</a> for the primary documentation</p>
      </body>
    </html>
    """.dedent

    exec("echo \"" & contents & "\" > " & paramStr(9) & "/index.html")
  else:
    exec("nim doc --project --index:on --outdir:docs src/flatbuffers")
    exec("nim doc --project --index:on --outdir:docs src/flatbuffers/private/flatnim")

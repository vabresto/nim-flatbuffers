## This file is autogenerated. Do not modify.
import std/options
import std/strformat
import flatbuffers


type Weapon* = object of TableBase
  ## To access members, exclude the `Schema` suffix of the desired member.
  nameSchema: string
  damageSchema: int16


func name*(t: Weapon): string =
  let val = get[string](t, 0.uint16)
  if val.isSome:
    val.get
  else:
    ""


func damage*(t: Weapon): int16 =
  let val = get[int16](t, 1.uint16)
  if val.isSome:
    val.get
  else:
    0.int16


func `==`*(l, r: Weapon): bool =
  l.name == r.name and
  l.damage == r.damage


func `$`*(t: Weapon): string =
  "(" &
    "name: \"" & t.name & "\"" & ", " &
    fmt"damage: {t.damage}" &
  ")"


type WeaponVec* = VectorBase[Weapon]


func `[]`*[I: Ordinal](v: WeaponVec, idx: I): Weapon =
  if idx >= v.size:
    raise newException(ValueError, "Index " & $idx & " is greater than vector size of " & $v.size)

  let dataStart = v.home.uint32 + 4  # +4 bytes because the start of the vec is its size
  let vectorStart = (dataStart + idx * 4).uint32.ufixed
  var ptrOffset: ufixed
  read[ufixed](v.buf, vectorStart, ptrOffset)
  let finalAddress = vectorStart + ptrOffset
  getAddressAs[Weapon](v.buf, finalAddress)


func `$`*(v: WeaponVec): string =
  result = "["
  for idx in 0 ..< v.size:
    result &= $v[idx]
    if idx != v.size - 1:
      result &= ", "
  result &= "]"


func toSeq*(v: WeaponVec): seq[Weapon] =
  result = newSeq[Weapon](v.size)

  for idx in 0 ..< v.size:
    result[idx] = v[idx]


func `==`*(l, r: WeaponVec): bool =
  if l.size != r.size:
    false
  else:
    for idx in 0 ..< l.size:
      if l[idx] != r[idx]:
        return false
    true


func getRootAsWeapon*(b: FlatBuffer): Weapon =
  getRootAs[Weapon](b)


import unittest

import std/options

import flatbuffers/private/parser {.all.}

test "parse include":
  var p = initParser("""
  include "myfile.txt";
  """)
  check p.parseIncludeDecl == Token(kind: tkIncludeDecl, lexeme: "myfile.txt")

test "parse namespace":
  var p = initParser("""
  namespace hello.world.from.nim;
  """)
  check p.parseNamespaceDecl == Token(kind: tkNamespaceDecl,
                                  namespace: @["hello", "world", "from", "nim"])

test "parse attribute decl":
  var p = initParser("""
  attribute "priority";
  """)
  check p.parseAttributeDecl == Token(kind: tkAttributeDecl, lexeme: "priority")

test "parse various scalars":
  var p: Parser = " true ".initParser
  check p.parseScalar == Scalar(kind: skBool, original: "true", valueBool: true)

  p = " 4821314345 ".initParser
  check p.parseScalar == Scalar(kind: skInt, original: "4821314345", valueInt: 4821314345.int)

  p = " 123.45678 ".initParser
  check p.parseScalar == Scalar(kind: skFloat, original: "123.45678", valueFloat: 123.45678)

test "parse metadata":
  var p: Parser = """ (id : 2, hello:   "world", deprecated) """.initParser
  check p.parseMetadata == @[
    Metadata(ident: "id", value: some SingleValue(kind: svScalar, scalar: Scalar(kind: skInt,
                                                                                 original: "2",
                                                                                 valueInt: 2))),
    Metadata(ident: "hello", value: some SingleValue(kind: svString, str: "world")),
    Metadata(ident: "deprecated", value: none SingleValue)
  ]

test "parse field":
  var p: Parser = """friendly:bool = false (deprecated, priority: 1);""".initParser
  check p.parseFieldDecl == Field(
    ident: "friendly",
    fieldType: Type(kind: tBool),
    default: some "false",
    metadata: @[
      Metadata(ident: "deprecated", value: none SingleValue),
      Metadata(ident: "priority", value: some SingleValue(kind: svScalar,
                                                          scalar: Scalar(kind: skInt,
                                                                         original: "1",
                                                                         valueInt: 1)))
    ]
  )

test "parse struct":
  var p: Parser = """
  
  struct Vec3 {
    x:float;
    y:float;
    z:float;
  }
  
  """.initParser
  discard p.parseObjectDecl

test "parse table":
  var p: Parser = """
  
  table Monster {
    pos:Vec3;
    mana:short = 150;
    hp:short = 100;
    name:string;
    friendly:bool = false (deprecated, priority: 1);
    inventory:[ubyte];
    color:Color = Blue;
    test:Any;
  }
  
  """.initParser
  discard p.parseObjectDecl

test "parse enum":
  var p: Parser = """

  // test comment
  enum Color : byte { Red = 1, Green, Blue }
  
  """.initParser
  discard p.parseEnumDecl

test "parse file":
  discard """
  // Example IDL file for our monster's schema.

  namespace MyGame.Sample;

  enum Color:byte { Red = 0, Green, Blue = 2 }

  union Equipment { Weapon } // Optionally add more tables.

  struct Vec3 {
    x:float;
    y:float;
    z:float;
  }

  table Monster {
    pos:Vec3; // Struct.
    mana:short = 150;
    hp:short = 100;
    name:string;
    friendly:bool = false (deprecated);
    inventory:[ubyte];  // Vector of scalars.
    color:Color = Blue; // Enum.
    weapons:[Weapon];   // Vector of tables.
    equipped:Equipment; // Union.
  }

  table Weapon {
    name:string;
    damage:short;
  }

  // root_type Monster;
  
  """.parseSchema

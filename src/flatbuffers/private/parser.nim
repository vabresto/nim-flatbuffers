## Implements a Flatbuffer parser for the grammar available at https://google.github.io/flatbuffers/flatbuffers_grammar.html

import std/algorithm
import std/options
import std/os
import std/parseutils
import std/re
import std/strutils
import std/sugar

import ./tokens
export tokens
import ./registry


{.experimental: "strictfuncs".}


type
  Parser = object
    input: string
    idx: uint64

    # For debug printing only
    curLineNum: uint64

  FlatbufferParseException* = object of ValueError


template rest(p: var Parser): lent string = p.input[p.idx .. ^1]
template here(p: var Parser): char =
  if p.idx < p.input.len.uint64:
    p.input[p.idx]
  else:
    raise newException(FlatbufferParseException,
                        "Parser tried to read out-of-bounds position " & $p.idx)

func peekNextWord(p: var Parser): string =
  var done = p.idx
  while done < p.input.len.uint64 and not p.input[done].isSpaceAscii:
    inc done
  p.input[p.idx..<done]

template expectChar(p: var Parser, c: char) =
  if p.here != c:
    raise newException(FlatbufferParseException,
                        "Expected '" & c & "' at position " & $p.idx & " (got '" & p.peekNextWord & "')")
  inc p.idx
template expectationViolated(expectation: string) =
  raise newException(FlatbufferParseException,
                     "Expected " & expectation & " at input position " & $p.idx &
                     " (input line " & $p.curLineNum & ") but got '" & p.peekNextWord & "'!")


func initParser(data: string): Parser =
  ## Create a new Flatbuffer parser on the given string
  if data.len == 0:
    raise newException(FlatbufferParseException, "initParser received empty string!")
  Parser(input: data, idx: 0, curLineNum: 1)


func consumeWhitespace(p: var Parser) =
  ## Flatbuffer schema is whitespace insensitive, so we need to
  ## make sure that we allow arbitrary spacing
  while p.idx < p.input.len.uint64 and p.here.isSpaceAscii:
    if p.here == '\n':
      inc p.curLineNum
    inc p.idx

  # also eat any comments
  if p.rest.startsWith("//"):
    # Skip to end of current line
    while p.idx < p.input.len.uint64 and p.here != '\n':
      inc p.idx

    # Recurse to capture whitespace after comments, or another comment
    p.consumeWhitespace


func parseStringConstant(p: var Parser): string {.raises: [FlatbufferParseException].} =
  ## Parses a quoted string
  p.consumeWhitespace
  try:
    if p.rest =~ re"""\"(.*?)\".*""":
      let data = matches[0]
      p.idx += (data.len + 2).uint64  # +2 for the quotes
      return data
    else:
      expectationViolated("string constant")
  except RegexError:
    expectationViolated("string constant")


func parseIdentString(p: var Parser): string {.raises: [FlatbufferParseException].} =
  ## Parses an identifier as a string (ie. not as a token)
  p.consumeWhitespace
  try:
    if p.rest =~ re"""([a-zA-Z_][a-zA-Z0-9_]*).*""":
      let data = matches[0]
      p.idx += data.len.uint64  # +2 for the quotes
      return data
    else:
      expectationViolated("identifier")
  except RegexError:
    expectationViolated("identifier")


func parseBool(p: var Parser): (string, bool) {.raises: [FlatbufferParseException].} =
  p.consumeWhitespace
  let rest = p.rest
  if rest.startsWith("true"):
    p.idx += 4
    ("true", true)
  elif rest.startsWith("false"):
    p.idx += 5
    ("false", false)
  else:
    expectationViolated("bool")


func parseInteger(p: var Parser): (string, int) {.raises: [FlatbufferParseException].} =
  p.consumeWhitespace
  let rest = p.rest
  try:
    if rest =~ re"""([-+]?[0-9]+).*""":
      let original = matches[0]
      let value = original.parseInt
      p.idx += original.len.uint64
      (original, value)
    elif rest =~ re"""([-+]?0[xX][0-9a-fA-F]+).*""":
      let original = matches[0]
      let value = original.parseHexInt
      p.idx += original.len.uint64
      (original, value)
    else:
      expectationViolated("integer")
  except RegexError, ValueError:
    expectationViolated("integer")


func parseFloat(p: var Parser): (string, BiggestFloat) {.raises: [FlatbufferParseException].} =
  p.consumeWhitespace
  let rest = p.rest
  try:
    # Dec float constant
    if rest =~ re"""([-+]?(([.][0-9]+)|([0-9]+[.][0-9]*)|([0-9]+))([eE][-+]?[0-9]+)?).*""":
      let original = matches[0]
      var value: BiggestFloat
      if original.parseBiggestFloat(value) != original.len:
        raise newException(ValueError, "")  # will be caught in this func
      p.idx += original.len.uint64
      (original, value)

    # Hex float constant
    elif rest =~ re"""([-+]?0[xX](([.][0-9a-fA-F]+)|([0-9a-fA-F]+[.][0-9a-fA-F]*)|([0-9a-fA-F]+))([pP][-+]?[0-9]+)).*""":
      debugEcho "Parsing hex float constants is not currently supported! :("
      quit 1

    # Special float constant
    elif rest =~ re"""([-+]?(nan|inf|infinity)).*""":
      let original = matches[0]
      # Nim supports "inf" but not "infinity" to float
      let value = original.replace("infinity", "inf").parseFloat
      p.idx += original.len.uint64
      (original, value)

    else:
      expectationViolated("float")
  except RegexError, ValueError:
    expectationViolated("float")


func parseScalar(p: var Parser): Scalar =
  ## Parse a (numeric) scalar (ie. bool/int/float)
  try:
    let (original, value) = p.parseBool
    Scalar(kind: skBool, original: original, valueBool: value)
  except FlatbufferParseException:

    # At this point, need to try to parse as int or float
    # There are a few ways of doing this, but the one that
    # requires the least knowledge of other state is to try
    # to parse as both, and take the one that parsed more (or didn't fail)

    let initialIndex = p.idx

    # Parse int
    let parseIntAttempt = try:
      let (original, value) = p.parseInteger
      some Scalar(kind: skInt, original: original, valueInt: value)
    except FlatbufferParseException:
      none Scalar

    p.idx = initialIndex

    # Parse float
    let parseFloatAttempt = try:
      let (original, value) = p.parseFloat
      some Scalar(kind: skFloat, original: original, valueFloat: value)
    except FlatbufferParseException:
      none Scalar

    # Check anything parsed
    if parseIntAttempt.isNone and parseFloatAttempt.isNone:
      expectationViolated("scalar value")
    else:
      if parseIntAttempt.isNone:
        parseFloatAttempt.get
      elif parseFloatAttempt.isNone:
        parseIntAttempt.get
      else:
        # Choose the one that parsed more
        if parseIntAttempt.get.original.len >= parseFloatAttempt.get.original.len:
          parseIntAttempt.get
        else:
          parseFloatAttempt.get


func parseIncludeDecl(p: var Parser): Token {.raises: [FlatbufferParseException].} =
  ## Parse an include statement
  p.consumeWhitespace
  if p.rest.startsWith("include"):
    p.idx += 7
    
    let s = p.parseStringConstant
    p.consumeWhitespace
    p.expectChar ';'
    
    Token(kind: tkIncludeDecl, lexeme: s)
  else:
    expectationViolated("string literal 'include'")


func parseFileExtDecl(p: var Parser): Token {.raises: [FlatbufferParseException].} =
  ## Parse a file type statement
  p.consumeWhitespace
  if p.rest.startsWith("file_extension"):
    p.idx += 14
    
    let s = p.parseStringConstant
    p.consumeWhitespace
    p.expectChar ';'
    
    Token(kind: tkFileExtensionDecl, lexeme: s)
  else:
    expectationViolated("string literal 'file_extension'")


func parseFileIdDecl(p: var Parser): Token {.raises: [FlatbufferParseException].} =
  ## Parse a file id statement
  p.consumeWhitespace
  if p.rest.startsWith("file_identifier"):
    p.idx += 15
    
    let s = p.parseStringConstant
    p.consumeWhitespace
    p.expectChar ';'
    
    Token(kind: tkFileIdentifierDecl, lexeme: s)
  else:
    expectationViolated("string literal 'file_identifier'")


func parseRootTypeDecl(p: var Parser): Token {.raises: [FlatbufferParseException].} =
  ## Parse a root type statement
  p.consumeWhitespace
  if p.rest.startsWith("root_type"):
    p.idx += 9
    
    let s = p.parseIdentString
    p.consumeWhitespace
    p.expectChar ';'
    
    Token(kind: tkRootTypeDecl, lexeme: s)
  else:
    expectationViolated("string literal 'root_type'")


func parseNamespaceDecl(p: var Parser): Token {.raises: [FlatbufferParseException].} =
  ## Parse a namespace statement
  p.consumeWhitespace
  if p.rest.startsWith("namespace"):
    p.idx += 9
    var idents: seq[string] = @[]

    while true:
      idents.add p.parseIdentString
      if p.here == '.':
        inc p.idx
      else:
        break

    p.consumeWhitespace
    p.expectChar ';'

    Token(kind: tkNamespaceDecl, namespace: idents)
  else:
    expectationViolated("string literal 'namespace'")


func parseAttributeDecl(p: var Parser): Token {.raises: [FlatbufferParseException].} =
  ## Parse an attribute statement
  p.consumeWhitespace
  if p.rest.startsWith("attribute"):
    p.idx += 9
    
    let s = try:
      let res = p.parseStringConstant
      p.consumeWhitespace
      res
    except FlatbufferParseException:
      try:
        let res = p.parseIdentString
        p.consumeWhitespace
        res
      except FlatbufferParseException:
        expectationViolated("attribute declaration")
  
    p.consumeWhitespace
    p.expectChar ';'

    Token(kind: tkAttributeDecl, lexeme: s)
  else:
    expectationViolated("string literal 'attribute'")


func parseMetadata(p: var Parser): seq[Metadata] {.raises: [FlatbufferParseException].} =
  ## Parse metadata attributes
  p.consumeWhitespace

  # Grammar defines metadata as OPTIONAL; if the next
  # char isn't '(', just return empty list here
  if p.here != '(':
    return @[]

  p.expectChar '('

  var metadata: seq[Metadata] = @[]
  while true:
    let ident = p.parseIdentString
    p.consumeWhitespace

    if p.here == ':':
      # Parse value too
      inc p.idx
      p.consumeWhitespace
      try:
        let scalar = p.parseScalar
        metadata.add Metadata(ident: ident, value: some SingleValue(kind: svScalar, scalar: scalar))
      except FlatbufferParseException:
        try:
          let str = p.parseStringConstant
          metadata.add Metadata(ident: ident, value: some SingleValue(kind: svString, str: str))
        except FlatbufferParseException:
          expectationViolated("metadata value")
    else:
      metadata.add Metadata(ident: ident, value: none SingleValue)

    p.consumeWhitespace
    if p.here == ',':
      inc p.idx
    else:
      break

  p.consumeWhitespace
  p.expectChar ')'
  metadata


func parseFlatType(p: var Parser): Type {.raises: [FlatbufferParseException].} =
  ## Parse a basic field type
  p.consumeWhitespace
  let rest = p.rest
  
  var sortedKinds = collect:
    for basicType in TypeKind:
      if basicType == tList or basicType == tIdent:
        continue
      basicType
  sortedKinds.sort((x, y) => (($x).len < ($y).len))

  for basicType in sortedKinds:
    if basicType == tList or basicType == tIdent:
      continue
    if rest.startsWith($basicType):
      p.idx += ($basicType).len.uint64
      return Type(kind: basicType)
  
  try:
    let ident = p.parseIdentString
    return Type(kind: tIdent, underlying:ident)
  except FlatbufferParseException:
    expectationViolated("flat type")


func parseType(p: var Parser): Type {.raises: [FlatbufferParseException].} =
  p.consumeWhitespace
  try:
    return p.parseFlatType
  except FlatbufferParseException:
    discard

  # Not a basic type; could still be a list
  try:
    p.expectChar '['
    let listType = p.parseFlatType
    p.consumeWhitespace
    p.expectChar ']'
    case listType.kind
    of tList:
      raise newException(FlatbufferParseException,
                          "Received nested list type at " & $p.idx & "! This is not allowed by the Flatbuffer spec.")
    of tIdent:
      return Type(kind: tList, underlying: listType.underlying)
    of tBool,tByte,tUbyte,tShort,tUshort,tInt,tUint,
      tFloat,tLong,tUlong,tDouble,tInt8,tUint8,tInt16,
      tUint16,tInt32,tUint32,tInt64,tUint64,tFloat32,
      tFloat64,tString:
        return Type(kind: tList, underlying: $listType.kind)
  except:
    expectationViolated("type")


func parseFieldDecl(p: var Parser): Field {.raises: [FlatbufferParseException].} =
  let ident = p.parseIdentString
  p.consumeWhitespace
  p.expectChar ':'
  let fieldType = p.parseType
  p.consumeWhitespace
  
  # Default value is optional
  # Grammar says default can only be a scalar, and scalars are numeric only
  # but some examples use identifiers ... We will just save the raw value here
  # and try to interpret it later
  var default = none string
  if p.here == '=':
    try:
      p.expectChar '='
      p.consumeWhitespace
      default = some p.peekNextWord
      if default.get.endsWith(";"):
        default.get.removeSuffix(";")
      p.idx += default.get.len.uint64
    except FlatbufferParseException:
      expectationViolated("default value")

  let metadata = p.parseMetadata
  p.consumeWhitespace
  p.expectChar ';'

  Field(ident: ident, fieldType: fieldType, default: default, metadata: metadata)


func parseObjectDecl(p: var Parser): Token {.raises: [FlatbufferParseException].} =
  ## Parse a struct or table
  p.consumeWhitespace
  let rest = p.rest
  let objType = if rest.startsWith("table"):
    p.idx += 5
    tkTable
  elif rest.startsWith("struct"):
    p.idx += 6
    tkStruct
  else:
    expectationViolated("string literal 'table' or 'struct'")

  let ident = p.parseIdentString
  let metadata = p.parseMetadata

  p.consumeWhitespace
  p.expectChar '{'

  var fields: seq[Field] = @[]
  # Must have at least one
  while true:
    fields.add p.parseFieldDecl
    p.consumeWhitespace

    if p.here == '}':
      break

  p.expectChar '}'

  if objType == tkStruct:
    Token(kind: tkStruct, structName: ident, structMetadata: metadata, structFields: fields)
  elif objType == tkTable:
    Token(kind: tkTable, tableName: ident, tableMetadata: metadata, tableFields: fields)
  else:
    expectationViolated("a table or struct")


func parseEnumDecl(p: var Parser): Token {.raises: [FlatbufferParseException].} =
  p.consumeWhitespace
  let rest = p.rest
  if not rest.startsWith("enum"):
    expectationViolated("string literal 'enum'")
  p.idx += 4
  
  let ident = p.parseIdentString
  p.consumeWhitespace
  p.expectChar ':'
  let enumType = p.parseFlatType
  if enumType.kind == tIdent:
    expectationViolated("enum base type to be a simple type, but was identifier '" & enumType.underlying & "'")

  let metadata = p.parseMetadata

  p.consumeWhitespace
  p.expectChar '{'
  var foundAnything = false
  var values: seq[EnumValue] = @[]
  while true:
    p.consumeWhitespace
    let valueIdent = try:
      p.parseIdentString
    except FlatbufferParseException:
      if foundAnything:
        ""
      else:
        expectationViolated("enum to have contents, but was empty")

    if valueIdent == "":
      break

    foundAnything = true
    var valueValue = none int64

    p.consumeWhitespace
    if p.here == '=':
      try:
        p.expectChar '='
        valueValue = some p.parseInteger()[1].int64
      except FlatbufferParseException:
        expectationViolated("enum default to be an integer")

    values.add EnumValue(ident: valueIdent, value: valueValue)

    p.consumeWhitespace
    if p.here == ',':
      inc p.idx
    else:
      break

  p.consumeWhitespace
  p.expectChar '}'

  Token(kind: tkEnumDecl, enumName: ident, enumType: enumType, enumValues: values, enumMetadata: metadata)


func parseUnionDecl(p: var Parser): Token {.raises: [FlatbufferParseException].} =
  p.consumeWhitespace
  let rest = p.rest
  if not rest.startsWith("union"):
    expectationViolated("string literal 'union'")
  p.idx += 5
  
  let ident = p.parseIdentString
  p.consumeWhitespace

  let metadata = p.parseMetadata

  p.consumeWhitespace
  p.expectChar '{'
  var values: seq[string] = @[]
  while true:
    p.consumeWhitespace
    let valueIdent = p.parseIdentString

    p.consumeWhitespace
    if p.here == '=':
      # Unclear why the grammar allows defaults for unions
      expectationViolated("next union member but got a default value; defaults are not supported for unions")

    values.add valueIdent

    p.consumeWhitespace
    if p.here == ',':
      inc p.idx
    else:
      break

  p.consumeWhitespace
  p.expectChar '}'

  Token(kind: tkUnionDecl, unionName: ident, unionValues: values, unionMetadata: metadata)


func parseSchema*(data: string): (seq[Token], Registry) {.raises: [FlatbufferParseException].} =
  var p = initParser(data)
  var registry = initRegistry()
  # Includes must be first
  var tokens: seq[Token] = @[]
  while true:
    try:
      let t = p.parseIncludeDecl
      tokens.add t
      registry.register t
    except FlatbufferParseException:
      break

  # No more includes here, try parsing everything else
  var curIdx = p.idx
  while true:
    curIdx = p.idx
    try:
      let t = p.parseNamespaceDecl
      tokens.add t
      registry.register t
      registry.setActiveNamespace t.namespace
    except FlatbufferParseException:
      discard

    try:
      let t = p.parseFileExtDecl
      tokens.add t
      registry.register t
    except FlatbufferParseException:
      discard

    try:
      let t = p.parseFileIdDecl
      tokens.add t
      registry.register t
    except FlatbufferParseException:
      discard

    try:
      let t = p.parseRootTypeDecl
      tokens.add t
      registry.register t
    except FlatbufferParseException:
      discard

    try:
      let t = p.parseAttributeDecl
      tokens.add t
      registry.register t
    except FlatbufferParseException:
      discard

    try:
      let t = p.parseObjectDecl
      tokens.add t
      registry.register t
    except FlatbufferParseException:
      discard

    try:
      let t = p.parseEnumDecl
      tokens.add t
      registry.register t
    except FlatbufferParseException:
      discard

    try:
      let t = p.parseUnionDecl
      tokens.add t
      registry.register t
    except FlatbufferParseException:
      discard

    # etc.

    if p.idx == p.input.len.uint64:
      # Done!
      break

    if p.idx == curIdx:
      # We made no progress!
      expectationViolated("a namespace, type, enum, attr, etc.")

  # Resolve all member fields
  try:
    for token in tokens:
      case token.kind
      of tkStruct:
        for field in token.structFields:
          registry.register field, token.structName
      of tkTable:
        for field in token.tableFields:
          registry.register field, token.tableName
      else:
        discard
  except KeyError, ValueError:
    {.noSideEffect.}:
      debugEcho getCurrentExceptionMsg()
      quit 1

  (tokens, registry)


proc recursivelyResolveIncludes*(startFile: string): string =
  ## Resolves includes ... recursively :)
  ## 
  ## Note: Does not currently handle cycles gracefully
  var contents = startFile.readFile
  while true:
    try:
      var p = contents.initParser
      let t = p.parseIncludeDecl
      let (dir, _, _) = startFile.splitFile
      let innerResolved = (dir & "/" & t.lexeme).recursivelyResolveIncludes

      # Cut out everything before and including the include so
      # we don't process it again
      contents = p.rest

      # Move new contents to the end because includes must be at the start
      contents &= "\n" & innerResolved
    except FlatbufferParseException:
      break
  contents

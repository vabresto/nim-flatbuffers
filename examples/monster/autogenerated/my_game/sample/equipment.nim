## This file is autogenerated. Do not modify.
import std/options
import std/strformat
import flatbuffers
import ./weapon
export weapon


type EquipmentKind* {.pure.} = enum
  ekNone = 0
  ekWeapon


type Equipment* = object of UnionBase[EquipmentKind]
  case kind*: EquipmentKind
  of EquipmentKind.ekNone:
    discard
  of EquipmentKind.ekWeapon:
    weapon*: Weapon


func `==`*(l, r: Equipment): bool =
  if l.kind == r.kind:
    case l.kind
    of EquipmentKind.ekNone:
      true
    of EquipmentKind.ekWeapon:
      l.weapon == r.weapon
  else:
    false


func `$`*(u: Equipment): string =
  "<Equipment>(" &
  (case u.kind
  of EquipmentKind.ekNone:
    "None"
  of EquipmentKind.ekWeapon:
    "Weapon: " & $u.weapon
  ) & ")"


type EquipmentVec* = VectorBase[Equipment]


func `[]`*[I: Ordinal](v: EquipmentVec, idx: I): Equipment =
  if idx >= v.size:
    raise newException(ValueError, "Index " & $idx & " is greater than vector size of " & $v.size)

  let dataStart = v.home.uint32 + 4  # +4 bytes because the start of the vec is its size
  let vectorStart = (dataStart + idx * 4).uint32.ufixed
  var ptrOffset: ufixed
  read[ufixed](v.buf, vectorStart, ptrOffset)
  let finalAddress = vectorStart + ptrOffset
  getAddressAs[Equipment](v.buf, finalAddress)


func `$`*(v: EquipmentVec): string =
  result = "["
  for idx in 0 ..< v.size:
    result &= $v[idx]
    if idx != v.size - 1:
      result &= ", "
  result &= "]"


func toSeq*(v: EquipmentVec): seq[Equipment] =
  result = newSeq[Equipment](v.size)

  for idx in 0 ..< v.size:
    result[idx] = v[idx]


func `==`*(l, r: EquipmentVec): bool =
  if l.size != r.size:
    false
  else:
    for idx in 0 ..< l.size:
      if l[idx] != r[idx]:
        return false
    true


func getRootAsEquipment*(b: FlatBuffer): Equipment =
  getRootAs[Equipment](b)


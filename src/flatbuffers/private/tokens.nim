import std/options
import std/sets
import std/strformat
import std/strutils


type
  TypeKind* = enum
    tBool = "bool"
    tByte = "byte"
    tUbyte = "ubyte"
    tShort = "short"
    tUshort = "ushort"
    tInt = "int"
    tUint = "uint"
    tFloat = "float"
    tLong = "long"
    tUlong = "ulong"
    tDouble = "double"
    tInt8 = "int8"
    tUint8 = "uint8"
    tInt16 = "int16"
    tUint16 = "uint16"
    tInt32 = "int32"
    tUint32 = "uint32"
    tInt64 = "int64"
    tUint64 = "uint64"
    tFloat32 = "float32"
    tFloat64 = "float64"
    tString = "string"
    tList = "list"
    tIdent = "ident"

  Type* = object
    case kind*: TypeKind
    of tIdent, tList:
      underlying*: string
    of tBool,tByte,tUbyte,tShort,tUshort,tInt,tUint,
       tFloat,tLong,tUlong,tDouble,tInt8,tUint8,tInt16,
       tUint16,tInt32,tUint32,tInt64,tUint64,tFloat32,
       tFloat64,tString:
      discard
    

  ScalarKind* = enum
    skBool
    skInt
    skFloat

  Scalar* = object
    original*: string
    case kind*: ScalarKind
    of skBool:
      valueBool*: bool
    of skInt:
      valueInt*: int
    of skFloat:
      valueFloat*: BiggestFloat


  SingleValueKind* = enum
    svScalar
    svString

  SingleValue* = object
    case kind*: SingleValueKind
    of svScalar:
      scalar*: Scalar
    of svString:
      str*: string


  Metadata* = object
    ident*: string
    value*: Option[SingleValue]

  EnumValue* = object
    ident*: string
    value*: Option[int64]

  Field* = object
    ident*: string
    fieldType*: Type
    default*: Option[string]
    metadata*: seq[Metadata]


  TokenKind* = enum
    tkIncludeDecl
    tkNamespaceDecl
    tkFileExtensionDecl
    tkFileIdentifierDecl
    tkRootTypeDecl
    tkAttributeDecl
    tkEnumDecl
    tkUnionDecl
    tkStruct
    tkTable

  Token* = object
    case kind*: TokenKind
    of tkIncludeDecl, tkAttributeDecl, tkFileExtensionDecl, tkFileIdentifierDecl, tkRootTypeDecl:
      lexeme*: string
    of tkNamespaceDecl:
      namespace*: seq[string]
    of tkEnumDecl:
      enumName*: string
      enumType*: Type
      enumValues*: seq[EnumValue]
      enumMetadata*: seq[Metadata]
    of tkUnionDecl:
      unionName*: string
      unionValues*: seq[string]
      unionMetadata*: seq[Metadata]
    of tkStruct:
      structName*: string
      structMetadata*: seq[Metadata]
      structFields*: seq[Field]
    of tkTable:
      tableName*: string
      tableMetadata*: seq[Metadata]
      tableFields*: seq[Field]


func `==`*(l, r: Type): bool =
  if l.kind == r.kind:
    case l.kind
    of tList, tIdent:
      l.underlying == r.underlying
    of tBool,tByte,tUbyte,tShort,tUshort,tInt,tUint,
       tFloat,tLong,tUlong,tDouble,tInt8,tUint8,tInt16,
       tUint16,tInt32,tUint32,tInt64,tUint64,tFloat32,
       tFloat64,tString:
      true
  else:
    false


func `==`*(l, r: Scalar): bool =
  if l.kind == r.kind:
    case l.kind
    of skBool:
      l.valueBool == r.valueBool
    of skInt:
      l.valueInt == r.valueInt
    of skFloat:
      l.valueFloat == r.valueFloat
  else:
    false


func `==`*(l, r: SingleValue): bool =
  if l.kind == r.kind:
    case l.kind
    of svScalar:
      l.scalar == r.scalar
    of svString:
      l.str == r.str
  else:
    false


func `==`*(l, r: Token): bool =
  if l.kind == r.kind:
    case l.kind
    of tkIncludeDecl, tkAttributeDecl, tkFileExtensionDecl, tkFileIdentifierDecl, tkRootTypeDecl:
      l.lexeme == r.lexeme
    of tkNamespaceDecl:
      l.namespace == r.namespace
    of tkEnumDecl:
      l.enumName == r.enumName and l.enumValues == r.enumValues and l.enumMetadata == r.enumMetadata
    of tkUnionDecl:
      l.unionName == r.unionName and l.unionValues == r.unionValues and l.unionMetadata == r.unionMetadata
    of tkStruct:
      l.structName == r.structName and l.structMetadata == r.structMetadata and l.structFields == r.structFields
    of tkTable:
      l.tableName == r.tableName and l.tableMetadata == r.tableMetadata and l.tableFields == r.tableFields
  else:
    false


func innerToNimType(t: string): string =
    if t == "bool": "bool"
    elif t == "byte": "byte"
    elif t == "ubyte": "byte"
    elif t == "short": "int16"
    elif t == "ushort": "uint16"
    elif t == "int": "int"
    elif t == "uint": "uint"
    elif t == "float": "float32"
    elif t == "long": "int64"
    elif t == "ulong": "uint64"
    elif t == "double": "float64"
    elif t == "int8": "int8"
    elif t == "uint8": "uint8"
    elif t == "int16": "int16"
    elif t == "uint16": "uint16"
    elif t == "int32": "int32"
    elif t == "uint32": "uint32"
    elif t == "int64": "int64"
    elif t == "uint64": "uint64"
    elif t == "float32": "float32"
    elif t == "float64": "float64"
    else: t


func toNimType*(t: Type): string =
  case t.kind
  of tList:
    if t.underlying.innerToNimType notin ["string", "bool", "byte", "int8", "uint8", "int16", "uint16",
                                          "int32", "uin32", "int64", "uint64", "int", "uint", "float32",
                                          "float64"].toHashSet:
      fmt"{t.underlying.innerToNimType.capitalizeAscii}Vec"
    else:
      fmt"VectorBase[{t.underlying.innerToNimType}]"
  of tIdent: t.underlying
  else: ($t.kind).innerToNimType


func contentsToNimType*(t: Type): string =
  case t.kind
  of tList: t.underlying.innerToNimType
  else:
    ""


func getDefaultValueStr*(f: Field): string =
  # Not sure how to make this generic in the type the field holds
  # without making things very complicated
  if f.default.isSome:
    f.default.get
  else:
    case f.fieldType.kind
    of tIdent, tList:
      f.fieldType.underlying
    of tBool,tByte,tUbyte,tShort,tUshort,tInt,tUint,
        tFloat,tLong,tUlong,tDouble,tInt8,tUint8,tInt16,
        tUint16,tInt32,tUint32,tInt64,tUint64,tFloat32,
        tFloat64:
      fmt"0.{f.fieldType.toNimType}"
    of tString:
      "\"\""


func isMarkedDeprecated*(f: Field): bool =
  for md in f.metadata:
    if md.ident == "deprecated":
      return true
  return false


func size*(f: Field): uint32 =
  case f.fieldType.kind
  of tIdent:
    # Not sure
    8
  of tList, tString:
    # Also Tables
    4
  of tBool,tByte,tUbyte,tInt8,tUint8:
    1
  of tShort,tUshort,tInt16,tUint16:
    2
  of tInt,tUint,tFloat,tInt32,tUint32,tFloat32:
    4
  of tLong,tUlong,tDouble,tInt64,tUint64,tFloat64:
    8

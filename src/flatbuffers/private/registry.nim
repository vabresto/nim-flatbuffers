# The registry is responsible for tracking types and their dependencies
import std/algorithm
import std/options
import std/tables
import std/sequtils
import std/strutils
import std/sugar

import ./tokens

type
  RegisteredTypeKind* = enum
    rtEnum
    rtTable
    rtStruct
    rtVector
    rtUnion
    rtVectorUnion
    rtAttribute
    rtBool
    rtByte
    rtInt
    rtUint
    rtInt8
    rtUint8
    rtInt16
    rtUint16
    rtInt32
    rtUint32
    rtInt64
    rtUint64
    rtFloat32
    rtFloat64
    rtString

  RegisteredType* = object
    case kind*: RegisteredTypeKind
    of rtUnion, rtEnum:
      enumPrefix*: string
    of rtStruct, rtTable:
      fieldNames*: seq[string]
    else:
      discard
    name*: string
    namespace*: seq[string]
    case fullyResolved*: bool
    of false:
      underlying*: seq[string]
    of true:
      discard

  Registry* = object
    types: Table[string, RegisteredType]
    values: Table[string, RegisteredType]
    activeNamespace: seq[string]


func initRegistry*(): Registry =
  Registry(types: initTable[string, RegisteredType]())


func `$`*(r: Registry): string =
  "(\n\ntypes: " & $r.types & " \n\nvalues: " & $r.values & ")"


func setActiveNamespace*(r: var Registry, n: seq[string]) =
  r.activeNamespace = n


func lookupType*(r: Registry, s: string): Option[RegisteredType] =
  if s in r.types:
    some r.types[s]
  elif s == "bool":
    some RegisteredType(kind: rtBool, name: s, fullyResolved: true)
  elif s == "byte":
    some RegisteredType(kind: rtByte, name: s, fullyResolved: true)
  elif s == "int8":
    some RegisteredType(kind: rtInt8, name: s, fullyResolved: true)
  elif s == "ubyte" or s == "uint8":
    some RegisteredType(kind: rtUint8, name: s, fullyResolved: true)
  elif s == "short" or s == "int16":
    some RegisteredType(kind: rtInt16, name: s, fullyResolved: true)
  elif s == "ushort" or s == "uint16":
    some RegisteredType(kind: rtUint16, name: s, fullyResolved: true)
  elif s == "int" or s == "int32":
    some RegisteredType(kind: rtInt32, name: s, fullyResolved: true)
  elif s == "uint" or s == "uint32":
    some RegisteredType(kind: rtUint32, name: s, fullyResolved: true)
  elif s == "long" or s == "int64":
    some RegisteredType(kind: rtInt64, name: s, fullyResolved: true)
  elif s == "ulong" or s == "uint64":
    some RegisteredType(kind: rtUint64, name: s, fullyResolved: true)
  elif s == "float" or s == "float32":
    some RegisteredType(kind: rtFloat32, name: s, fullyResolved: true)
  elif s == "float64" or s == "double":
    some RegisteredType(kind: rtFloat64, name: s, fullyResolved: true)
  elif s == "string":
    some RegisteredType(kind: rtString, name: s, fullyResolved: true)
  else:
    none RegisteredType


func lookupValue*(r: Registry, s: string): Option[RegisteredType] =
  if s in r.values:
    some r.values[s]
  else:
    none RegisteredType


func register*(r: var Registry, f: Field, parent: string) =
  case f.fieldType.kind
  of tIdent:
    let resolved = r.lookupType f.fieldType.underlying
    if resolved.isSome:
      r.values[parent & "." & f.ident] = RegisteredType(kind: resolved.get.kind,
                                                        name: f.ident,
                                                        namespace: resolved.get.namespace,
                                                        fullyResolved: false,
                                                        underlying: @[resolved.get.name])
    else:
      raise newException(ValueError, "Unable to resolve type of " & parent & "." & f.ident)
  of tList:
    let resolved = r.lookupType f.fieldType.underlying
    if resolved.isSome:
      r.values[parent & "." & f.ident] = RegisteredType(kind: rtVector,
                                                        name: f.ident,
                                                        namespace: resolved.get.namespace,
                                                        fullyResolved: false,
                                                        underlying: @[resolved.get.name])
    else:
      raise newException(ValueError, "Unable to resolve type of " & parent & "." & f.ident)
  of tBool:
    r.values[parent & "." & f.ident] = RegisteredType(kind: rtBool, name: f.ident, fullyResolved: true)
  of tByte:
    r.values[parent & "." & f.ident] = RegisteredType(kind: rtByte, name: f.ident, fullyResolved: true)
  of tInt8:
    r.values[parent & "." & f.ident] = RegisteredType(kind: rtInt8, name: f.ident, fullyResolved: true)
  of tUbyte, tUint8:
    r.values[parent & "." & f.ident] = RegisteredType(kind: rtUint8, name: f.ident, fullyResolved: true)
  of tShort, tInt16:
    r.values[parent & "." & f.ident] = RegisteredType(kind: rtInt16, name: f.ident, fullyResolved: true)
  of tUshort, tUint16:
    r.values[parent & "." & f.ident] = RegisteredType(kind: rtUint16, name: f.ident, fullyResolved: true)
  of tInt, tInt32:
    r.values[parent & "." & f.ident] = RegisteredType(kind: rtInt32, name: f.ident, fullyResolved: true)
  of tUint, tUint32:
    r.values[parent & "." & f.ident] = RegisteredType(kind: rtUint32, name: f.ident, fullyResolved: true)
  of tLong, tInt64:
    r.values[parent & "." & f.ident] = RegisteredType(kind: rtInt64, name: f.ident, fullyResolved: true)
  of tUlong, tUint64:
    r.values[parent & "." & f.ident] = RegisteredType(kind: rtUint64, name: f.ident, fullyResolved: true)
  of tFloat, tFloat32:
    r.values[parent & "." & f.ident] = RegisteredType(kind: rtFloat32, name: f.ident, fullyResolved: true)
  of tDouble, tFloat64:
    r.values[parent & "." & f.ident] = RegisteredType(kind: rtFloat64, name: f.ident, fullyResolved: true)
  of tString:
    r.values[parent & "." & f.ident] = RegisteredType(kind: rtString, name: f.ident, fullyResolved: true)


func makeEnumPrefix(s: string): string =
  # k for kind
  s.filter(x => x.isUpperAscii).join("").toLowerAscii & "k"


func register*(r: var Registry, t: Token) =
  case t.kind
  of tkEnumDecl:
    r.types[t.enumName] = RegisteredType(kind: rtEnum,
                                        name: t.enumName,
                                        namespace: r.activeNamespace,
                                        enumPrefix: t.enumName.makeEnumPrefix,
                                        fullyResolved: false,
                                        underlying: @[$t.enumType.kind])
  of tkAttributeDecl:
    r.types[t.lexeme] = RegisteredType(kind: rtAttribute,
                                      name: t.lexeme,
                                      fullyResolved: true)
  of tkStruct:
    var fieldNames: seq[string] = @[]
    for field in t.structFields:
      fieldNames.add field.ident

    r.types[t.structName] = RegisteredType(kind: rtStruct,
                                          name: t.structName,
                                          namespace: r.activeNamespace,
                                          fieldNames: fieldNames,
                                          fullyResolved: true)
  of tkTable:
    var fieldNames: seq[string] = @[]
    for field in t.tableFields:
      if field.isMarkedDeprecated:
        continue
      fieldNames.add field.ident

    r.types[t.tableName] = RegisteredType(kind: rtTable,
                                          name: t.tableName,
                                          namespace: r.activeNamespace,
                                          fieldNames: fieldNames,
                                          fullyResolved: true)
  of tkUnionDecl:
    r.types[t.unionName] = RegisteredType(kind: rtUnion,
                                          name: t.unionName,
                                          namespace: r.activeNamespace,
                                          enumPrefix: t.unionName.makeEnumPrefix,
                                          fullyResolved: false,
                                          underlying: t.unionValues)
  of tkIncludeDecl, tkNamespaceDecl, tkFileExtensionDecl, tkFileIdentifierDecl, tkRootTypeDecl:
    discard


func dependsDirectlyOn*(r: Registry, query: string): seq[RegisteredType] =
  let tOpt = r.lookupType query
  result = @[]
  if tOpt.isNone:
    return result
  else:
    let t = tOpt.get
    case t.kind
    of rtStruct, rtTable:
      for field in t.fieldNames:
        let fOpt = r.lookupValue t.name & "." & field
        if fOpt.isNone:
          continue
        case fOpt.get.kind
        of rtEnum, rtTable, rtStruct:
          result.add fOpt.get
        of rtVector, rtVectorUnion:
          for subtype in fOpt.get.underlying:
            let sOpt = r.lookupType subtype
            if sOpt.isSome:
              case sOpt.get.kind
              of rtEnum:
                # Set enums to fullyResolved = true at this point, so
                # that we don't try to import their underlying type,
                # which is always a primitive, so it will fail
                result.add RegisteredType(kind: rtEnum,
                                          enumPrefix: sOpt.get.enumPrefix,
                                          name: sOpt.get.name,
                                          namespace: sOpt.get.namespace,
                                          fullyResolved: true)
              else:
                result.add sOpt.get
        of rtUnion:
          result.add fOpt.get
          for subtype in fOpt.get.underlying:
            let sOpt = r.lookupType subtype
            if sOpt.isSome:
              result.add sOpt.get
        else:
          continue
      return result
    of rtUnion:
      result.add t
    else:
      return result


func toFileCase*(s: string): string =
  result = ""
  var isFirst = true
  for c in s:
    if c.isUpperAscii:
      if not isFirst:
        result &= "_"
      result &= c.toLowerAscii
    else:
      result &= c
    isFirst = false


func makeFileName*(namespace: seq[string], name: string, ext: string = ""): string =
  namespace.map(toFileCase).join("/") & "/" & name.toFileCase & (if ext != "": "." & ext else: "")


func createImportStatements*(reg: Registry, parentType: string, parentNamespace: seq[string]): seq[string] =
  let types = reg.dependsDirectlyOn(parentType)
  var exports = newSeq[string]()
  for rt in types:
    let moduleName = if not rt.fullyResolved:
      rt.underlying[0].toFileCase
    else:
      case rt.kind
      of rtStruct, rtTable:
        rt.name.toFileCase
      of rtVector:
        rt.underlying[0].toFileCase
      else:
        continue

    if parentType.toFileCase == moduleName:
      # Don't try to import self
      break

    if rt.namespace == parentNamespace:
      result.add "import ./" & moduleName
      exports.add "export " & moduleName
    else:
      var namespaceUp = newSeq[string]()
      var namespaceDown = newSeq[string]()

      for (l, r) in zip(parentNamespace.reversed, rt.namespace.reversed):
        if l != r:
          namespaceUp.add ".."
          namespaceDown.add r
        else:
          break

      result.add "import " & makeFileName(namespaceUp & namespaceDown, moduleName)
      exports.add "export " & moduleName
  result = result.sorted.deduplicate(isSorted=true)
  result &= exports.sorted.deduplicate(isSorted=true)

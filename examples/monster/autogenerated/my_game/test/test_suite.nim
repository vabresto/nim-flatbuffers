## This file is autogenerated. Do not modify.
import std/options
import std/strformat
import flatbuffers
import ./test_case
export test_case


type TestSuite* = object of TableBase
  ## To access members, exclude the `Schema` suffix of the desired member.
  nameSchema: string
  casesSchema: TestCaseVec


func name*(t: TestSuite): string =
  let val = get[string](t, 0.uint16)
  if val.isSome:
    val.get
  else:
    ""


func cases*(t: TestSuite): TestCaseVec =
  let val = get[TestCaseVec](t, 1.uint16)
  if val.isSome:
    val.get
  else:
    TestCaseVec()


func `==`*(l, r: TestSuite): bool =
  l.name == r.name and
  l.cases == r.cases


func `$`*(t: TestSuite): string =
  "(" &
    "name: \"" & t.name & "\"" & ", " &
    fmt"cases: {t.cases}" &
  ")"


type TestSuiteVec* = VectorBase[TestSuite]


func `[]`*[I: Ordinal](v: TestSuiteVec, idx: I): TestSuite =
  if idx >= v.size:
    raise newException(ValueError, "Index " & $idx & " is greater than vector size of " & $v.size)

  let dataStart = v.home.uint32 + 4  # +4 bytes because the start of the vec is its size
  let vectorStart = (dataStart + idx * 4).uint32.ufixed
  var ptrOffset: ufixed
  read[ufixed](v.buf, vectorStart, ptrOffset)
  let finalAddress = vectorStart + ptrOffset
  getAddressAs[TestSuite](v.buf, finalAddress)


func `$`*(v: TestSuiteVec): string =
  result = "["
  for idx in 0 ..< v.size:
    result &= $v[idx]
    if idx != v.size - 1:
      result &= ", "
  result &= "]"


func toSeq*(v: TestSuiteVec): seq[TestSuite] =
  result = newSeq[TestSuite](v.size)

  for idx in 0 ..< v.size:
    result[idx] = v[idx]


func `==`*(l, r: TestSuiteVec): bool =
  if l.size != r.size:
    false
  else:
    for idx in 0 ..< l.size:
      if l[idx] != r[idx]:
        return false
    true


func getRootAsTestSuite*(b: FlatBuffer): TestSuite =
  getRootAs[TestSuite](b)

